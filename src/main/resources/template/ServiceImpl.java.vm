package ${package}.${moduleName}.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import ${package}.${moduleName}.utils.ModelUtil;
import ${package}.${moduleName}.enums.ServiceCode;
import com.seaboxdata.commons.exception.ServiceException;
import com.seaboxdata.commons.query.PaginationResult;
import ${package}.${moduleName}.mapper.${className}Mapper;
import ${package}.${moduleName}.model.${className};
import ${package}.${moduleName}.service.I${className}Service;
import ${package}.${moduleName}.vo.${className}VO;
import ${package}.${moduleName}.dto.${className}DTO;
import ${package}.${moduleName}.dto.${className}PageDTO;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;


@Service("${classname}Service")
public class ${className}ServiceImpl extends ServiceImpl<${className}Mapper, ${className}> implements I${className}Service {

    @Autowired
    private ${className}Mapper mapper;

    @Override
    public PaginationResult<${className}VO> ${classname}Page(${className}PageDTO ${classname}PageDTO) {
        if (null == ${classname}PageDTO) {
            return null;
        }
        IPage<${className}> page = new Page<>(1 + (${classname}PageDTO.getOffset() / ${classname}PageDTO.getLimit()), ${classname}PageDTO.getLimit());
        IPage<${className}> iPage = mapper.selectPage(page, new QueryWrapper<>());
        PaginationResult<${className}VO> paginationResult = new PaginationResult<>();
        if (iPage == null) {
            return null;
        }
        paginationResult.setOffset((int) iPage.getCurrent());
        paginationResult.setLimit((int) iPage.getSize());
        paginationResult.setTotal((int) iPage.getTotal());
        ArrayList<${className}VO> ${classname}VOs = new ArrayList<>();
        List<${className}> records = iPage.getRecords();
        ModelUtil.copyList(records, ${classname}VOs, ${className}VO.class);

        paginationResult.setData(${classname}VOs);
        return paginationResult;
    }

    @Override
    public List<${className}VO> ${classname}List(${className}DTO ${classname}DTO) {
        List<${className}> ${classname}s = this.lambdaQuery().list();
        ArrayList<${className}VO> dtos = new ArrayList<>();
        ModelUtil.copyList(${classname}s, dtos, ${className}VO.class);

        return dtos;
    }

    @Override
    public Boolean ${classname}Save(${className}DTO ${classname}DTO) {
        if (${classname}DTO == null) {
            throw new ServiceException(ServiceCode.SERVICE_CODE_403, "dto" + ServiceCode.DATA_IS_EMPTY);
        }
        ${className} ${classname} = new ${className}();
        BeanUtils.copyProperties(${classname}DTO, ${classname});
        return this.save(${classname});
    }

    @Override
    public Boolean ${classname}Update(${className}DTO ${classname}DTO) {
        ${className} ${classname} = new ${className}();
        BeanUtils.copyProperties(${classname}DTO, ${classname});
        return this.updateById(${classname});
    }

#if(${pk})
    @Override
    public ${className}VO ${classname}Detail(${pk.attrType} ${pk.attrname}) {
        ${className}VO ${classname}VO = new ${className}VO();
        ${className} ${classname} = this.getById(${pk.attrname});
        if (${classname} == null) {
            return null;
        }
        BeanUtils.copyProperties(${classname}, ${classname}VO);
        return ${classname}VO;
    }
#end
}
